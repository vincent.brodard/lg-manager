package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class AliveAdapter extends RecyclerView.Adapter<AliveAdapter.ViewHolder> {

    private final RoleListManager rlm;
    private CemeteryAdapter cemeteryAdapter;
    private final EventFragment eventFragment;

    static class ViewHolder extends RecyclerView.ViewHolder {
        final Button killButton;
        private final TextView name;
        private final TextView nbPlayerAlive;

        ViewHolder(View v) {
            super(v);
            killButton = v.findViewById(R.id.killButton);
            name = v.findViewById(R.id.roleTextAlive);
            nbPlayerAlive = v.findViewById(R.id.nbPlayerAlive);
        }

        void bind(final RoleListManager rlm, final int position) {
            final Role currentRole = rlm.activeRoles.get(position);
            name.setText(rlm.activeRoles.get(position).getName());
            nbPlayerAlive.setText(String.valueOf(currentRole.getCount()-currentRole.getDeadCount()));
            if(!currentRole.isActive()){
                itemView.setVisibility(View.GONE);
            }
        }
    }

    void setCemeteryAdapter(CemeteryAdapter cemeteryAdapter){
        this.cemeteryAdapter=cemeteryAdapter;
    }

    AliveAdapter(RoleListManager rlm,EventFragment eventFragment) {
        this.rlm = rlm;
        this.eventFragment=eventFragment;
    }

    @NonNull
    @Override
    public AliveAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_alive_cemetery, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bind(rlm, position);
        holder.killButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Role crtRole = rlm.activeRoles.get(position);
                crtRole.killOne();
                rlm.updateActiveRoles();
                rlm.updateDeadRoles();
                notifyDataSetChanged();
                cemeteryAdapter.notifyDataSetChanged();
                rlm.checkVictoryCondition(holder.itemView.getContext());
                if (!MainActivity.isMuted) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(v.getContext(), R.raw.scream);
                    mediaPlayer.start();
                }

                String historyNewText="Mort : ";
                historyNewText=historyNewText.concat(crtRole.getName());
                historyNewText=historyNewText.concat(" durant ");
                historyNewText=historyNewText.concat(eventFragment.crtEventLabel);

                MainActivity.historyManager.addText(historyNewText);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rlm.activeRoles.size();
    }
}
