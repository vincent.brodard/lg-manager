package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class CemeteryAdapter extends RecyclerView.Adapter<CemeteryAdapter.ViewHolder> {

    private final RoleListManager rlm;
    private AliveAdapter aliveAdapter;
    private final EventFragment eventFragment;

    static class ViewHolder extends RecyclerView.ViewHolder {
        final Button resurrectButton;
        private final TextView name;
        private final TextView nbPlayerDead;
        ViewHolder(View v) {
            super(v);
            resurrectButton = v.findViewById(R.id.resurrectButton);
            name = v.findViewById(R.id.roleTextCemetery);
            nbPlayerDead = v.findViewById(R.id.nbPlayerDead);
        }

        void bind(final RoleListManager rlm, final int position) {
            Role currentRole = rlm.deadRoles.get(position);
            name.setText(currentRole.getName());
            nbPlayerDead.setText(String.valueOf(currentRole.getDeadCount()));
            if(!currentRole.isOneOrMoreDead()) {
                itemView.setVisibility(View.GONE);
            }
        }
    }

    void setAliveAdapter(AliveAdapter aliveAdapter){
        this.aliveAdapter=aliveAdapter;
    }

    CemeteryAdapter(RoleListManager rlm, EventFragment eventFragment) {
        this.rlm = rlm;
        this.eventFragment=eventFragment;
    }

    @NonNull
    @Override
    public CemeteryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dead_cemetery, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.bind(rlm, position);
        holder.resurrectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Role crtRole = rlm.deadRoles.get(position);
                crtRole.ressurectOne();
                rlm.updateActiveRoles();
                rlm.updateDeadRoles();
                notifyDataSetChanged();
                aliveAdapter.notifyDataSetChanged();
                if (!MainActivity.isMuted) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(eventFragment.getContext(), R.raw.resurrection);
                    mediaPlayer.start();
                }

                String historyNewText="Ressuscite : ";
                historyNewText=historyNewText.concat(crtRole.getName());
                historyNewText=historyNewText.concat(" durant ");
                historyNewText=historyNewText.concat(eventFragment.crtEventLabel);

                MainActivity.historyManager.addText(historyNewText);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rlm.deadRoles.size();
    }
}
