package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

@SuppressLint("ValidFragment")
public class CemeteryFragment extends Fragment {

    private final EventFragment eventFragment;
    private final RoleListManager rlm;

    @SuppressLint("ValidFragment")
    public CemeteryFragment(RoleListManager rlm,EventFragment eventFragment){
        this.rlm=rlm;
        this.eventFragment=eventFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.cemetery, container, false);

        //Alive Recycler View
        RecyclerView aliveRecyclerView = view.findViewById(R.id.aliveList);
        aliveRecyclerView.setHasFixedSize(true);
        LinearLayoutManager aliveLayoutManager = new LinearLayoutManager(view.getContext());
        aliveRecyclerView.setLayoutManager(aliveLayoutManager);
        AliveAdapter aliveAdapter = new AliveAdapter(rlm, eventFragment);
        aliveRecyclerView.setAdapter(aliveAdapter);

        //Cemetery Recycler View
        RecyclerView cemeteryRecyclerView = view.findViewById(R.id.deadList);
        cemeteryRecyclerView.setHasFixedSize(true);
        LinearLayoutManager cemeteryLayoutManager = new LinearLayoutManager(view.getContext());
        cemeteryRecyclerView.setLayoutManager(cemeteryLayoutManager);
        CemeteryAdapter cemeteryAdapter = new CemeteryAdapter(rlm,eventFragment);
        cemeteryRecyclerView.setAdapter(cemeteryAdapter);

        //Lier les fragments entre eux
        aliveAdapter.setCemeteryAdapter(cemeteryAdapter);
        cemeteryAdapter.setAliveAdapter(aliveAdapter);
        aliveAdapter.notifyDataSetChanged();
        cemeteryAdapter.notifyDataSetChanged();

        //Back to Event button
        Button resume = view.findViewById(R.id.resumeButton);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.fragmentManager.popBackStack("Cemetery fragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                eventFragment.goToNextValideEventOrStayIfCurrentIsValide(eventFragment.eventFragmentView);
            }
        });

        return view;
    }
}
