package ch.hefr.tic.lg_manager;

class Constants {
    static final int NOMBRE_DEFAUT_PAR_ROLE = 0;
    static final int NOMBRE_MINIMUM_PAR_ROLE = 0;
    static final int NOMBRE_MAXIMUM_PAR_ROLE = 99;
    static final int TIME_MINUTE_DEFAULT = 3;
    static final int TIMER_MINUTE_MIN = 0;
    static final int TIMER_MINUTE_MAX = 99;
    static final int TIME_SECOND_DEFAULT = 0;
    static final int TIMER_SECOND_MIN = 0;
    static final int TIMER_SECOND_MAX = 59;
    static final int TIMER_DELAY = 0;
    static final int TIMER_PERIOD = 1000;
}
