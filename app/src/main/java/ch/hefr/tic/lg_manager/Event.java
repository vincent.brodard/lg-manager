package ch.hefr.tic.lg_manager;

import java.util.ArrayList;
import java.util.List;

class Event {

    private final Phases phase;
    private final String description;
    private final Role role;
    private final int image;
    private final List<Information> informations;
    private final String mainInformation;
    private final boolean isRequired;

    Event(Phases phase, String description, Role role, int image, String mainInformation, boolean isRequired, List<Information> informations){
        this.phase=phase;
        this.description=description;
        this.role=role;
        this.image=image;
        this.mainInformation=mainInformation;
        this.isRequired=isRequired;
        this.informations= new ArrayList<>(informations);
    }

    Phases getPhase(){return phase;}

    String getDescription() {
        return description;
    }

    Role getRole() {
        return role;
    }

    int getImage() {
        return image;
    }

    String getMainInformation() {
        return mainInformation;
    }

    boolean isNotRequired() {
        return !isRequired;
    }

    List<Information> getInformations() {
        return informations;
    }
}
