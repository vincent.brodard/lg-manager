package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

@SuppressLint("ValidFragment")
public class EventFragment extends Fragment {

    private final RoleListManager rlm;
    private EventListManager elm;

    private int nightCount = 0;
    private int dayCount = 0;
    private boolean isPrepEnded = false;
    private boolean isAngeEnded = false;

    private LinearLayout image_or_timer_layout;

    private LinearLayout.LayoutParams params;

    private TextView timerTextView;
    private View timerView;

    private static int startTimerMinuteCount = Constants.TIME_MINUTE_DEFAULT;
    private static int currentTimerMinuteCount = Constants.TIME_MINUTE_DEFAULT;
    private static int startTimerSecondCount = Constants.TIME_SECOND_DEFAULT;
    private static int currentTimerSecondCount = Constants.TIME_SECOND_DEFAULT;

    private enum timerStates {
        RUNNING,
        PAUSED,
    }

    private static timerStates currentTimerState = timerStates.PAUSED;

    private static Timer timer = new Timer();

    public View eventFragmentView;

    public String crtEventLabel;

    @SuppressLint("ValidFragment")
    public EventFragment(RoleListManager rlm){
        this.rlm=rlm;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        elm=new EventListManager(rlm);
    }

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View myView = inflater.inflate(R.layout.event, container, false);

        inflater= LayoutInflater.from(myView.getContext());
        timerView=inflater.inflate(R.layout.timer_layout, null);
        ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);

        //Mise à jour de la liste des rôles
        rlm.updateActiveRoles();
        rlm.updateDeadRoles();
        rlm.checkVictoryCondition(myView.getContext());

        eventFragmentView = myView;

        updateView(myView);

        Button nextButton = myView.findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);
                resetTimer();
                updateListNext(myView);
            }
        });

        Button prevButton = myView.findViewById(R.id.previous_button);
        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);
                resetTimer();
                assert elm.events.peek() != null;
                if(!(elm.events.peek().getPhase()==Phases.DEBUT_PREPARATION)) {
                    updateListPrev(myView);
                }

            }
        });

        //Cemetery button
        Button cemetery = myView.findViewById(R.id.cemetery_button);
        cemetery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.eventContentContainer, MainActivity.cemeteryFragment)
                        .addToBackStack("Cemetery fragment")
                        .commit();
            }
        });

        ((TextView)myView.findViewById(R.id.informations)).setMovementMethod(new ScrollingMovementMethod());

        return myView;

    }

    private void updateListNext(View view){
        Event crtEvent = elm.events.peek();

        assert crtEvent != null;
        if(crtEvent.getPhase()==Phases.FIN_JOURNEE){
            nightCount++;
            if (!MainActivity.isMuted){
                MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.wolf);
                mediaPlayer.start();
            }
        }

        if(crtEvent.getPhase()==Phases.FIN_NUIT){
            dayCount++;
            if (!MainActivity.isMuted) {
                MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.cacarico);
                mediaPlayer.start();
            }
        }

        if(crtEvent.getPhase()==Phases.FIN_PREPARATION){
            isPrepEnded=true;

            if(rlm.roles.get(Roles.ANGE.getValue()).isActive()){
                //Jour de l'Ange
                if (!MainActivity.isMuted) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.wolf);
                    mediaPlayer.start();
                }
            }
            else{
                nightCount++;
                if (!MainActivity.isMuted) {
                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.cacarico);
                    mediaPlayer.start();
                }
            }
        }

        if(crtEvent.getPhase()==Phases.FIN_PREMIER_DEBAT_ET_VOTE_ANGE){
            isAngeEnded=true;
            nightCount++;
            if (!MainActivity.isMuted) {
                MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.cacarico);
                mediaPlayer.start();
            }
        }

        elm.events.add(crtEvent);
        elm.events.remove();

        goToNextValideEventOrStayIfCurrentIsValide(view);
    }

    public void goToNextValideEventOrStayIfCurrentIsValide(View view){
        Event crtEvent = elm.events.peek();

        assert crtEvent != null;
        while((crtEvent.getPhase()==Phases.PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.DEBUT_PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.FIN_PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.DEBUT_PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.FIN_PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.GRAND_MECHANT_LOUP && rlm.isOneOrMoreWolfDead()) ||
                (crtEvent.getPhase()==Phases.PREMIERE_NUIT && nightCount>1) ||
                (crtEvent.getPhase()==Phases.NUITS_PAIRES && nightCount%2!=0) ||
                (crtEvent.getPhase()==Phases.NUITS_IMPAIRES && nightCount%2!=1) ||
                (crtEvent.isNotRequired() && !crtEvent.getRole().isActive())){
            //On passe à l'événement suivant
            elm.events.add(crtEvent);
            elm.events.remove();
            crtEvent=elm.events.peek();
            assert crtEvent != null;
        }

        updateView(view);
    }

    private void updateListPrev(View view){
        Event crtEvent = elm.events.peek();
        assert crtEvent != null;

        if(crtEvent.getPhase()==Phases.DEBUT_JOURNEE){
            dayCount--;
        }

        if(crtEvent.getPhase()==Phases.DEBUT_NUIT){
            nightCount--;
        }

        if(dayCount==0 && nightCount==0){
            isPrepEnded=false;
            isAngeEnded=false;
        }

        Event prevEvent = elm.events.getLast();
        elm.events.removeLast();
        elm.events.addFirst(prevEvent);

        crtEvent = elm.events.peek();

        assert crtEvent != null;
        while((crtEvent.getPhase()==Phases.PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.DEBUT_PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.FIN_PREPARATION && isPrepEnded) ||
                (crtEvent.getPhase()==Phases.DEBUT_PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.FIN_PREMIER_DEBAT_ET_VOTE_ANGE && isAngeEnded) ||
                (crtEvent.getPhase()==Phases.GRAND_MECHANT_LOUP && rlm.isOneOrMoreWolfDead()) ||
                (crtEvent.getPhase()==Phases.PREMIERE_NUIT && nightCount>1) ||
                (crtEvent.getPhase()==Phases.NUITS_PAIRES && nightCount%2!=0) ||
                (crtEvent.getPhase()==Phases.NUITS_IMPAIRES && nightCount%2!=1) ||
                (crtEvent.isNotRequired() && !crtEvent.getRole().isActive())){
            //On passe à l'événement suivant
            prevEvent = elm.events.getLast();
            elm.events.removeLast();
            elm.events.addFirst(prevEvent);
            crtEvent=elm.events.peek();
            assert crtEvent != null;
        }

        updateView(view);
    }

    private void updateView(View view){
        Event crtEvent = elm.events.peek();

        assert crtEvent != null;
        Phases phase = crtEvent.getPhase();
        String description = crtEvent.getDescription();
        int image = crtEvent.getImage();
        String informations = crtEvent.getMainInformation();
        for(Information info : crtEvent.getInformations()){
            if (info.getConcernedRole().isActive()){
                informations = informations.concat("\n");
                informations = informations.concat(info.getTextInformation());
            }
        }

        ((TextView) view.findViewById(R.id.phase)).setText(phase.getLabel());

        if(phase==Phases.JOURNEE || phase==Phases.DEBUT_JOURNEE) {
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(R.drawable.sun);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(R.drawable.sun);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(R.drawable.day_background);

            //Compteur jours
            String phaseText = ((TextView) view.findViewById(R.id.phase)).getText().toString();
            phaseText=phaseText.concat("  :  ");
            phaseText=phaseText.concat(String.valueOf(dayCount));
            ((TextView) view.findViewById(R.id.phase)).setText(phaseText);

            crtEventLabel="la journée ";
            crtEventLabel=crtEventLabel.concat(String.valueOf(dayCount));
        }
        else if(phase==Phases.PREPARATION || phase==Phases.DEBUT_PREPARATION || phase==Phases.FIN_PREPARATION) {
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(android.R.color.transparent);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(android.R.color.transparent);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(android.R.color.transparent);

            crtEventLabel="la préparation";
        }
        else if(phase==Phases.PREMIER_DEBAT_ET_VOTE_ANGE || phase==Phases.DEBUT_PREMIER_DEBAT_ET_VOTE_ANGE ) {
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(R.drawable.sun);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(R.drawable.sun);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(R.drawable.day_background);

            crtEventLabel="le jour de l'Ange";
        }
        else if(phase==Phases.FIN_JOURNEE || phase==Phases.FIN_PREMIER_DEBAT_ET_VOTE_ANGE){
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(R.drawable.dusk);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(R.drawable.dusk);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(R.drawable.dusk_background);

            crtEventLabel="le crépuscule";
        }
        else if(phase==Phases.FIN_NUIT){
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(R.drawable.dawn);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(R.drawable.dawn);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(R.drawable.dawn_background);

            crtEventLabel="l'aube";
        }
        else {
            ((ImageView) view.findViewById(R.id.image_day_or_night_1)).setImageResource(R.drawable.moon);
            ((ImageView) view.findViewById(R.id.image_day_or_night_2)).setImageResource(R.drawable.moon);
            (view.findViewById(R.id.inner_layout)).setBackgroundResource(R.drawable.night_background);

            //Compteur nuits
            String phaseText = ((TextView) view.findViewById(R.id.phase)).getText().toString();
            phaseText=phaseText.concat("  :  ");
            phaseText=phaseText.concat(String.valueOf(nightCount));
            ((TextView) view.findViewById(R.id.phase)).setText(phaseText);

            crtEventLabel="la nuit ";
            crtEventLabel=crtEventLabel.concat(String.valueOf(nightCount));
        }

        ((TextView) view.findViewById(R.id.description)).setText(description);

        ImageView imageView = new ImageView(view.getContext());
        imageView.setImageResource(image);

        image_or_timer_layout = view.findViewById(R.id.timer_or_image_layout);
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        image_or_timer_layout.setGravity(Gravity.CENTER);
        image_or_timer_layout.removeAllViews();

        if(crtEvent.getDescription().equals("Débats")){
            handleTimer();
        }
        else{
            image_or_timer_layout.addView(imageView,params);
        }

        ((TextView) view.findViewById(R.id.informations)).setText(informations);
        (view.findViewById(R.id.informations)).scrollTo(0,0);
        (view.findViewById(R.id.informations)).setScrollbarFadingEnabled(false);
    }

    private void handleTimer(){

        @SuppressLint("DefaultLocale") final String timerText = String.format("%02d", startTimerMinuteCount).concat(":").concat(String.format("%02d", startTimerSecondCount));
        timerTextView = timerView.findViewById(R.id.timer_text);
        timerTextView.setText(timerText);
        image_or_timer_layout.addView(timerView,params);

        timerView.findViewById(R.id.timer_decr_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentTimerMinuteCount!=Constants.TIMER_MINUTE_MIN){
                    currentTimerMinuteCount--;
                    @SuppressLint("DefaultLocale") String timerText = String.format("%02d", currentTimerMinuteCount).concat(":").concat(String.format("%02d", currentTimerSecondCount));
                    timerTextView.setText(timerText);
                }
            }
        });

        timerView.findViewById(R.id.timer_incr_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentTimerMinuteCount!=Constants.TIMER_MINUTE_MAX){
                    currentTimerMinuteCount++;
                    @SuppressLint("DefaultLocale") String timerText = String.format("%02d", currentTimerMinuteCount).concat(":").concat(String.format("%02d", currentTimerSecondCount));
                    timerTextView.setText(timerText);
                }
            }
        });

        timerView.findViewById(R.id.timer_start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if(currentTimerState==timerStates.PAUSED) {
                    ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.stop);
                    currentTimerState=timerStates.RUNNING;
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            if (currentTimerSecondCount != Constants.TIMER_SECOND_MIN) {
                                currentTimerSecondCount--;
                            } else if (currentTimerMinuteCount != Constants.TIMER_MINUTE_MIN) {
                                currentTimerSecondCount = Constants.TIMER_SECOND_MAX;
                                currentTimerMinuteCount--;
                            } else {
                                Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        AlertDialog test = new AlertDialog.Builder(v.getContext())
                                                .setTitle("Le temps est écoulé")
                                                .setPositiveButton(R.string.fermer, null)
                                                .setIcon(R.drawable.timer)
                                                .show();
                                        test.setCanceledOnTouchOutside(false);
                                    }
                                });
                                if (!MainActivity.isMuted) {
                                    MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.tagueule);
                                    mediaPlayer.start();
                                }
                                ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);
                                resetTimer();
                            }
                            @SuppressLint("DefaultLocale") final String timerText = String.format("%02d", currentTimerMinuteCount).concat(":").concat(String.format("%02d", currentTimerSecondCount));
                            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    timerTextView.setText(timerText);
                                }
                            });

                        }
                    }, Constants.TIMER_DELAY, Constants.TIMER_PERIOD);
                }
                else{
                    ((Button)timerView.findViewById(R.id.timer_start_button)).setText(R.string.start);
                    currentTimerState=timerStates.PAUSED;
                    timer.cancel();
                }
            }
        });

        timerView.findViewById(R.id.timer_reset_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTimerMinuteCount=startTimerMinuteCount;
                currentTimerSecondCount=startTimerSecondCount;
                @SuppressLint("DefaultLocale") String timerText = String.format("%02d", currentTimerMinuteCount).concat(":").concat(String.format("%02d", currentTimerSecondCount));
                timerTextView.setText(timerText);
            }
        });
    }

    public static void resetTimer(){
        startTimerMinuteCount=Constants.TIME_MINUTE_DEFAULT;
        currentTimerMinuteCount=Constants.TIME_MINUTE_DEFAULT;
        startTimerSecondCount=Constants.TIME_SECOND_DEFAULT;
        currentTimerSecondCount=Constants.TIME_SECOND_DEFAULT;

        currentTimerState = timerStates.PAUSED;
        timer.cancel();
    }
}
