package ch.hefr.tic.lg_manager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class EventListManager {
    final LinkedList<Event> events=  new LinkedList<>();

    EventListManager(RoleListManager rlm) {
        events.add(new Event(Phases.DEBUT_PREPARATION, "Préparation de la partie",RoleListManager.aucunRole, android.R.color.transparent,"Préparez la partie en suivant les prochaines étapes.",true, new ArrayList<Information>()));

        events.add(new Event(Phases.PREPARATION, "Cartes Spiritisme",rlm.roles.get(Roles.GITANE.getValue()), R.drawable.gitane,"Préparez les cartes \"Spiritisme\" pour la Gitane.",false, new ArrayList<Information>()));
        events.add(new Event(Phases.PREPARATION, "Cartes Événement",RoleListManager.avecEvenements, R.drawable.garde_champetre,"Préparez les cartes événements que le Garde Champêtre lira.",false, new ArrayList<Information>()));
        events.add(new Event(Phases.PREPARATION, "Cartes pour le Voleur", rlm.roles.get(Roles.VOLEUR.getValue()), R.drawable.voleur, "Préparez deux cartes supplémentaires pour le Voleur.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREPARATION, "Cartes pour le Comédien", rlm.roles.get(Roles.COMEDIEN.getValue()), R.drawable.comedien, "Préparez trois cartes pour le Comédien.",false,new ArrayList<Information>()));
        List<Information> distribInfos = new ArrayList<>();
        distribInfos.add(new Information(rlm.roles.get(Roles.VOLEUR.getValue()),"Il y aura deux cartes de plus pour le Voleur, à mettre de côté."));
        events.add(new Event(Phases.PREPARATION, "Attribution des rôles",RoleListManager.aucunRole, R.drawable.dos_de_carte, "Distribuez les cartes personnages aux joueurs.",true,distribInfos));
        events.add(new Event(Phases.PREPARATION, "Attribution des bâtiments",RoleListManager.avecBatiments, android.R.color.transparent, "Distribuez les bâtiments aux joueurs.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREPARATION, "Abominable Sectaire", rlm.roles.get(Roles.ABOMINABLE_SECTAIRE.getValue()), R.drawable.abominable_sectaire, "Séparez le Village en deux camps distincts pour l'Abominable Sectaire.",false,new ArrayList<Information>()));
        List<Information> capitaineInfos = new ArrayList<>();
        capitaineInfos.add(new Information(RoleListManager.avecBatiments,"Le Capitaine est élu parmi les fermiers du Village."));
        events.add(new Event(Phases.PREPARATION, "Élections", RoleListManager.aucunRole, R.drawable.capitaine, "Le village doit choisir un Capitaine.",true,capitaineInfos));
        events.add(new Event(Phases.PREPARATION, "Garde Champêtre", RoleListManager.avecEvenements, R.drawable.garde_champetre, "Le Capitaine élit un Garde Champêtre.",false,new ArrayList<Information>()));

        events.add(new Event(Phases.FIN_PREPARATION, "Début de la partie",RoleListManager.aucunRole, android.R.color.transparent,"Annoncez aux joueurs que la partie va commencer.",true, new ArrayList<Information>()));

        events.add(new Event(Phases.DEBUT_PREMIER_DEBAT_ET_VOTE_ANGE, "Début du jour de l'Ange", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.ange, "Annoncez aux joueurs que l'Ange est présent.",false,new ArrayList<Information>()));

        events.add(new Event(Phases.PREMIER_DEBAT_ET_VOTE_ANGE, "Débats", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.ange, "Vous pouvez minuter les débats pour plus de dynamisme. Vous pouvez fair appliquer les règles de \"Une seule personne à la fois\".",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIER_DEBAT_ET_VOTE_ANGE, "Accusations", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.ange, "Demandez des noms d'accusés et des motifs d'accusation.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIER_DEBAT_ET_VOTE_ANGE, "Défenses", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.ange, "Chaque accusé se défend, chacun son tour.",false,new ArrayList<Information>()));
        List<Information> voteAngeInfos = new ArrayList<>();
        voteAngeInfos.add(new Information(rlm.roles.get(Roles.SERVANTE_DEVOUEE.getValue()),"Appel à la Servante Dévouée."));
        events.add(new Event(Phases.PREMIER_DEBAT_ET_VOTE_ANGE, "Vote", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.ange, "Précisez si les votes blancs sont autorisés ou non et faites un décompte de trois.",false,voteAngeInfos));

        events.add(new Event(Phases.FIN_PREMIER_DEBAT_ET_VOTE_ANGE, "La journée arrive à son terme.", rlm.roles.get(Roles.ANGE.getValue()), R.drawable.dusk, "Assurez-vous de n'avoir rien oublié.",false,new ArrayList<Information>()));

        events.add(new Event(Phases.DEBUT_NUIT, "La nuit tombe", RoleListManager.aucunRole, R.drawable.moon, "Demandez aux joueurs de fermer les yeux.",true,new ArrayList<Information>()));

        events.add(new Event(Phases.PREMIERE_NUIT, "Voleur",  rlm.roles.get(Roles.VOLEUR.getValue()), R.drawable.voleur, "Le Voleur choisit une carte parmi les deux écartées au début de la partie.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Ancien",  rlm.roles.get(Roles.ANCIEN.getValue()), R.drawable.ancien, "Retenez le joueur qui joue l'Ancien et prenez en note.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Petite Fille",  rlm.roles.get(Roles.PETITE_FILLE.getValue()), R.drawable.petite_fille, "Retenez le joueur qui joue la Petite Fille et prenez en note.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Montreur d'Ours",  rlm.roles.get(Roles.MONTREUR_D_OURS.getValue()), R.drawable.montreur_d_ours, "Retenez le joueur qui joue le Montreur d'Ours et prenez en note.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Cupidon",  rlm.roles.get(Roles.CUPIDON.getValue()), R.drawable.cupidon, "Il désigne deux personnes qui seront les Amoureux. Il a le droit de se désigner lui-même.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Amoureux",  rlm.roles.get(Roles.CUPIDON.getValue()), R.drawable.cupidon, "Ils doivent se protéger et essayer de gagner la partie ensemble. Ils ne doivent pas se porter préjudice.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Juge Bègue",  rlm.roles.get(Roles.JUGE_BEGUE.getValue()), R.drawable.juge_begue, "Retenez le joueur qui joue le Juge Bègue ainsi que le signe choisi et prenez en note.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.PREMIERE_NUIT, "Enfant Sauvage",  rlm.roles.get(Roles.ENFANT_SAUVAGE.getValue()), R.drawable.enfant_sauvage, "Retenez le joueur qui joue l'Enfant Sauvage et prenez en note.\nL'Enfant Sauvage choisit son modèle et est responsable d'y faire attention.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.NUITS_IMPAIRES, "Soeurs",  rlm.roles.get(Roles.SOEUR.getValue()), R.drawable.soeur, "Les Soeurs peuvent communiquer et s'échanger des informations.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.NUITS_IMPAIRES, "Frères",  rlm.roles.get(Roles.FRERE.getValue()), R.drawable.frere, "Les Frères peuvent communiquer et s'échanger des informations.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Comédien", rlm.roles.get(Roles.COMEDIEN.getValue()), R.drawable.comedien, "Le Comédien peut choisir une carte parmi les costumes restants. Hôtez la carte choisie.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Voyante", rlm.roles.get(Roles.VOYANTE.getValue()), R.drawable.voyante, "La Voyante observe la carte d'un autre joueur.",false,new ArrayList<Information>()));
        List<Information> corbeauInfos = new ArrayList<>();
        corbeauInfos.add(new Information(RoleListManager.avecBatiments,"Le Corbeau ne peut pas cibler un Vagabond."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Corbeau", rlm.roles.get(Roles.CORBEAU.getValue()), R.drawable.corbeau, "Déposez la marque du corbeau devant la personne ciblée.",false,corbeauInfos));
        List<Information> SalvateurInfos = new ArrayList<>();
        SalvateurInfos.add(new Information(rlm.roles.get(Roles.PETITE_FILLE.getValue()),"Sa protection ne fonctionne pas sur la Petite Fille."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Salvateur", rlm.roles.get(Roles.SALVATEUR.getValue()), R.drawable.salvateur, "Le salvateur peut se protéger lui-même.\nLe Salvateur ne peut pas protéger deux fois de suite la même personne.",false,SalvateurInfos));
        List<Information> pyromaneInfos = new ArrayList<>();
        pyromaneInfos.add(new Information(RoleListManager.avecBatiments,"Le Pyromane ne peut pas cibler un Vagabond."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Pyromane", rlm.roles.get(Roles.PYROMANE.getValue()), R.drawable.pyromane, "Ne déposez pas tout de suite la marque mais retenez la personne ciblée.",false,pyromaneInfos));
        List<Information> loupsInfos = new ArrayList<>();
        loupsInfos.add(new Information(rlm.roles.get(Roles.PETITE_FILLE.getValue()),"La Petite Fille guigne."));
        loupsInfos.add(new Information(rlm.roles.get(Roles.SALVATEUR.getValue()),"La victime ne meurt pas si elle est protégée par le Salvateur."));
        loupsInfos.add(new Information(rlm.roles.get(Roles.ANCIEN.getValue()),"La victime ne meurt pas si c'est l'Ancien et qu'il s'agit de sa première morsure."));
        loupsInfos.add(new Information(rlm.roles.get(Roles.PYROMANE.getValue()),"Attention à la cible du Pyromane."));
        loupsInfos.add(new Information(rlm.roles.get(Roles.INFECT_PERE_DES_LOUPS.getValue()),"Attention au signe de l'Infect Père des Loups (morsure sur l'épaule et retenir le joueur infecté."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Loups-Garous", RoleListManager.aucunRole, R.drawable.simple_loup_garou, "Retenez les joueurs qui jouent les Loups et prenez en note.\nIls doivent choisir une victime en étant tous d'accord.",true,loupsInfos));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Boulanger",RoleListManager.avecBatiments,android.R.color.transparent,"Le Boulanger ferme les yeux après avoir tenté d'observer des Loups.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.NUITS_PAIRES, "Loup-Garou Blanc", rlm.roles.get(Roles.LOUP_GAROU_BLANC.getValue()), R.drawable.loup_garou_blanc, "Il peut, s'il le désire, tuer un autre Loup-Garou.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.GRAND_MECHANT_LOUP, "Grand Méchant Loup", rlm.roles.get(Roles.GRAND_MECHANT_LOUP.getValue()), R.drawable.grand_mechant_loup, "Il dévore une deuxième victime seul.",false,new ArrayList<Information>()));
        List<Information> marquePyromaneInfos = new ArrayList<>();
        marquePyromaneInfos.add(new Information(RoleListManager.avecBatiments,"La personne ciblée ne devrait pas être un Vagabond."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Marque du Pyromane", rlm.roles.get(Roles.PYROMANE.getValue()), R.drawable.marque_du_pyromane, "Déposez maintenant l'insigne du pyromane devant la personne qui avait été ciblée par ce dernier.",false,marquePyromaneInfos));
        List<Information> SorciereInfos = new ArrayList<>();
        SorciereInfos.add(new Information(rlm.roles.get(Roles.SALVATEUR.getValue()),"Ne pas lui indiquer la victime si elle a été protégée par le Salvateur."));
        SorciereInfos.add(new Information(rlm.roles.get(Roles.PYROMANE.getValue()),"Ne pas lui indiquer la victime si elle a survécu grâce au Pyromane."));
        SorciereInfos.add(new Information(rlm.roles.get(Roles.INFECT_PERE_DES_LOUPS.getValue()),"Ne pas lui indiquer la victime si elle a été infectée."));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Sorcière", rlm.roles.get(Roles.SORCIERE.getValue()), R.drawable.sorciere, "Elle peut utiliser ses deux potions la même nuit si elle le désire.",false,SorciereInfos));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Renard", rlm.roles.get(Roles.RENARD.getValue()), R.drawable.renard, "Il choisit un groupe de trois personnes voisines en désignant la personne centrale.\n Il ne peut plus agir s'il découvre trois innocents.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Gitane", rlm.roles.get(Roles.GITANE.getValue()), R.drawable.gitane, "Elle peut utiliser une carte Spiritisme si elle le désire. Lisez à voix haute les quatre questions en les énumérant. Elle choisit ensuite une question et un Spirite.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Joueur de Flûte", rlm.roles.get(Roles.JOUEUR_DE_FLUTE.getValue()), R.drawable.joueur_de_flute, "Il désigne jusqu'à deux personnes.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.TOUTES_LES_NUITS, "Joueurs charmés", rlm.roles.get(Roles.JOUEUR_DE_FLUTE.getValue()), R.drawable.joueur_de_flute, "Ils se reconnaissent.",false,new ArrayList<Information>()));

        events.add(new Event(Phases.FIN_NUIT, "La nuit arrive à son terme. ", RoleListManager.aucunRole, R.drawable.dawn, "Assurez-vous de n'avoir rien oublié.",true,new ArrayList<Information>()));

        events.add(new Event(Phases.DEBUT_JOURNEE, "Le jour se lève", RoleListManager.aucunRole, R.drawable.sun, "Demandez aux joueurs d'ouvrir les yeux.",true,new ArrayList<Information>()));

        events.add(new Event(Phases.JOURNEE, "Victimes", RoleListManager.aucunRole, R.drawable.tombstone, "Révélez les victimes provoquées durant la nuit.",true,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Grognement de l'Ours", rlm.roles.get(Roles.MONTREUR_D_OURS.getValue()), R.drawable.montreur_d_ours, "Indiquez aux joueurs si l'Ours grogne ou non.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Marque du Corbeau", rlm.roles.get(Roles.CORBEAU.getValue()), R.drawable.marque_du_corbeau, "Dévoilez aux joueurs où se trouve la marque du Corbeau.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Costumes du Comédien", rlm.roles.get(Roles.COMEDIEN.getValue()), R.drawable.comedien, "Indiquez aux joueurs si un costume du Comédien est manquant ou non.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Spirite", rlm.roles.get(Roles.GITANE.getValue()), R.drawable.gitane, "Le Spirite lit à voix haute la question choisie par la Gitane sur la carte Spiritisme.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Événement", RoleListManager.avecEvenements, R.drawable.garde_champetre, "Le Garde Champêtre lit à voix haute la première carte Événement de la pile.",false,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Débats", RoleListManager.aucunRole,android.R.color.transparent, "Vous pouvez minuter les débats pour plus de dynamisme. Vous pouvez fair appliquer les règles de \"Une seule personne à la fois\".",true,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Accusations", RoleListManager.aucunRole, android.R.color.transparent, "Demandez des noms d'accusés et des motifs d'accusation.",true,new ArrayList<Information>()));
        events.add(new Event(Phases.JOURNEE, "Défenses", RoleListManager.aucunRole, android.R.color.transparent, "Chaque accusé se défend, chacun son tour.",true,new ArrayList<Information>()));
        List<Information> voteInfos = new ArrayList<>();
        voteInfos.add(new Information(rlm.roles.get(Roles.JUGE_BEGUE.getValue()),"Attention au signe du Juge Bègue."));
        voteInfos.add(new Information(rlm.roles.get(Roles.SERVANTE_DEVOUEE.getValue()),"Appel à la Servante Dévouée."));
        events.add(new Event(Phases.JOURNEE, "Vote", RoleListManager.aucunRole, android.R.color.transparent, "Précisez si les votes blancs sont autorisés ou non et faites un décompte de trois.",true,voteInfos));
        events.add(new Event(Phases.JOURNEE, "Eventuel second vote", rlm.roles.get(Roles.JUGE_BEGUE.getValue()), R.drawable.juge_begue, "Ce vote n'a lieu que si le Juge Bègue a utilisé son pouvoir. Il n'y a pas de débats avant ce vote.",false, new ArrayList<Information>()));

        events.add(new Event(Phases.FIN_JOURNEE, "La journée arrive à son terme.", RoleListManager.aucunRole, R.drawable.dusk, "Assurez-vous de n'avoir rien oublié.",true,new ArrayList<Information>()));
    }
}
