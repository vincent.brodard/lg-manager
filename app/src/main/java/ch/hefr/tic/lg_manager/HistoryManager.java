package ch.hefr.tic.lg_manager;

public class HistoryManager {
    private String historyText;

    HistoryManager(){
        this.historyText="";
    }

    String getHistoryText() {
        return historyText;
    }

    void addText(String newText){
        historyText=historyText.concat("\n");
        historyText=historyText.concat(newText);
    }
}
