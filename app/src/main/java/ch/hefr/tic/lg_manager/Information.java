package ch.hefr.tic.lg_manager;

class Information {
    private final Role concernedRole;
    private final String textInformation;

    Information(Role concernedRole, String textInformation){
        this.concernedRole=concernedRole;
        this.textInformation=textInformation;
    }

    Role getConcernedRole() {
        return concernedRole;
    }

    String getTextInformation() {
        return textInformation;
    }
}
