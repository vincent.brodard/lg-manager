package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.app.AlertDialog;

import android.content.DialogInterface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    public static boolean isMuted = false;
    private Toolbar toolbar;

    public static FragmentManager fragmentManager;
    @SuppressLint("StaticFieldLeak")
    public static EventFragment eventFragment;
    @SuppressLint("StaticFieldLeak")
    public static CemeteryFragment cemeteryFragment;

    public static HistoryManager historyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RoleListManager rlm = new RoleListManager(this);
        MenuFragment menuFragment = new MenuFragment(rlm);
        eventFragment = new EventFragment(rlm);
        cemeteryFragment = new CemeteryFragment(rlm,eventFragment);

        historyManager=new HistoryManager();

        //Remove the title on the toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(null);

        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction()
                .add(R.id.eventContentContainer, menuFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        if (isMuted){
            toolbar.getMenu().findItem(R.id.action_mute_unmute).setIcon(R.drawable.ic_volume_off);
        }else{
            toolbar.getMenu().findItem(R.id.action_mute_unmute).setIcon(R.drawable.ic_volume_up);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_mute_unmute) {
            isMuted = !isMuted;
            if (isMuted){
                toolbar.getMenu().findItem(R.id.action_mute_unmute).setIcon(R.drawable.ic_volume_off);
            }else{
                toolbar.getMenu().findItem(R.id.action_mute_unmute).setIcon(R.drawable.ic_volume_up);
            }
            return true;
        }

        if (id == R.id.action_restart) {
            new AlertDialog.Builder(this)
                    .setTitle("Redémarrer")
                    .setMessage("Voulez-vous vraiment recommencer une partie ?")

                    .setPositiveButton(R.string.Start_new_game, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            restart();
                        }
                    })
                    .show();

            return true;
        }

        if (id == R.id.action_quit) {
            AlertDialog quitDialog = new AlertDialog.Builder(this)
                    .setTitle("Quitter")
                    .setMessage("Voulez-vous vraiment quitter ?")

                    .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })

                    .setNegativeButton("Annuler",null)
                    .show();
            quitDialog.setCanceledOnTouchOutside(false);
            return true;
        }

        if (id == R.id.action_history){

            LayoutInflater inflater= LayoutInflater.from(this);
            @SuppressLint("InflateParams") View view=inflater.inflate(R.layout.history_layout, null);

            TextView textview=view.findViewById(R.id.history_text);
            textview.setMovementMethod(new ScrollingMovementMethod());
            textview.setText(historyManager.getHistoryText());

            Button history_button=view.findViewById(R.id.history_button);

            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Historique");
            alertDialog.setMessage("Voici toutes les actions effectuées jusqu'à présent :");
            alertDialog.setView(view);
            final AlertDialog alert = alertDialog.create();
            alert.show();

            history_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.cancel();
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }

    void restart(){
        EventFragment.resetTimer();
        finish();
        startActivity(getIntent());
    }
}
