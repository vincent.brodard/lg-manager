package ch.hefr.tic.lg_manager;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private final RoleListManager rlm;

    static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView image;
        private final Button incBT;
        private final TextView name;
        private final Button decrBT;
        private final TextView count;
        ViewHolder(View v) {
            super(v);
            image = v.findViewById(R.id.event_image);
            incBT = v.findViewById(R.id.incrButton);
            name = v.findViewById(R.id.name);
            decrBT = v.findViewById(R.id.decrButton);
            count = v.findViewById(R.id.count);
        }

        void bind(final RoleListManager rlm, final int position) {
            image.setImageResource(rlm.roles.get(position).getImage());
            incBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rlm.roles.get(position).countIncrement();
                    count.setText(String.valueOf(rlm.roles.get(position).getCount()));
                }
            });
            name.setText(rlm.roles.get(position).getName());
            decrBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rlm.roles.get(position).countDecrement();
                    count.setText(String.valueOf(rlm.roles.get(position).getCount()));
                }
            });
            count.setText(String.valueOf(rlm.roles.get(position).getCount()));
        }
    }

    MenuAdapter(RoleListManager rlm) {
        this.rlm = rlm;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(rlm, position);
    }

    @Override
    public int getItemCount() {
        return rlm.roles.size();
    }
}
