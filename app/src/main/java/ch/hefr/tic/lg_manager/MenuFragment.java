package ch.hefr.tic.lg_manager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

@SuppressLint("ValidFragment")
public class MenuFragment extends Fragment {

    private final RoleListManager rlm;

    @SuppressLint("ValidFragment")
    public MenuFragment(RoleListManager rlm){
        this.rlm=rlm;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.menu, container, false);

        RecyclerView mRecyclerView = view.findViewById(R.id.MenuList);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        MenuAdapter mAdapter = new MenuAdapter(rlm);

        mRecyclerView.setAdapter(mAdapter);

        RoleListManager.avecEvenements.setActive(false);
        RoleListManager.avecBatiments.setActive(false);

        RadioButton avecEvenementRadioButton = view.findViewById(R.id.avecEvenement);
        avecEvenementRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleListManager.avecEvenements.setActive(true);
            }
        });

        RadioButton sansEvenementRadioButton = view.findViewById(R.id.sansEvenement);
        sansEvenementRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleListManager.avecEvenements.setActive(false);
            }
        });

        RadioButton avecBatimenttRadioButton = view.findViewById(R.id.avecBatiment);
        avecBatimenttRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleListManager.avecBatiments.setActive(true);
            }
        });

        RadioButton sansBatimenttRadioButton = view.findViewById(R.id.sansBatiment);
        sansBatimenttRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoleListManager.avecBatiments.setActive(false);
            }
        });

        //Start button
        Button start = view.findViewById(R.id.startButton);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.fragmentManager.beginTransaction()
                        .replace(R.id.eventContentContainer, MainActivity.eventFragment)
                        .addToBackStack("Event fragment")
                        .commit();
            }
        });

        return view;
    }

}
