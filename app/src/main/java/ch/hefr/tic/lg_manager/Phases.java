package ch.hefr.tic.lg_manager;

public enum Phases {
    DEBUT_PREPARATION("Préparation"),
    PREPARATION("Préparation"),
    FIN_PREPARATION("Préparation"),
    TOUTES_LES_NUITS("Nuit"),
    PREMIERE_NUIT("Nuit"),
    NUITS_IMPAIRES("Nuit"),
    NUITS_PAIRES("Nuit"),
    GRAND_MECHANT_LOUP("Nuit"),
    DEBUT_PREMIER_DEBAT_ET_VOTE_ANGE("Jour de l'Ange"),
    PREMIER_DEBAT_ET_VOTE_ANGE("Jour de l'Ange"),
    FIN_PREMIER_DEBAT_ET_VOTE_ANGE("Jour de l'Ange"),
    DEBUT_NUIT("Nuit"),
    FIN_NUIT("Aube"),
    JOURNEE("Journée"),
    DEBUT_JOURNEE("Journée"),
    FIN_JOURNEE("Crépuscule");

    private final String displayedLabel;
    Phases(String displayedLabel) {
        this.displayedLabel = displayedLabel;
    }

    public String getLabel() {
        return displayedLabel;
    }
}
