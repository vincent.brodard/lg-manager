package ch.hefr.tic.lg_manager;

class Role {
    private final String name;
    private final int image;
    private int count;
    private int deadcount;
    private boolean isActive;
    private boolean isOneOrMoreDead;
    private final Camps camp;

    Role(String name, int image,Camps camp) {
        this.name = name;
        this.image = image;
        count = Constants.NOMBRE_DEFAUT_PAR_ROLE;
        deadcount = Constants.NOMBRE_MINIMUM_PAR_ROLE;
        isActive=false;
        isOneOrMoreDead=false;
        this.camp=camp;
    }

    public String getName () {
        return name;
    }

    int getImage() {
        return image;
    }

    int getCount() {
        return count;
    }

    int getDeadCount() {
        return deadcount;
    }

    boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    boolean isOneOrMoreDead() {
        return isOneOrMoreDead;
    }

    void killOne(){
        if(deadcount==count)return;

        deadcount++;
        if(deadcount==count){
            isActive=false;
        }
        isOneOrMoreDead=true;
    }

    void ressurectOne(){
        if(deadcount==Constants.NOMBRE_MINIMUM_PAR_ROLE)return;

        deadcount--;
        isActive=true;
        if(deadcount==Constants.NOMBRE_MINIMUM_PAR_ROLE){
            isOneOrMoreDead=false;
        }
    }

    void countIncrement() {
        if(count!=Constants.NOMBRE_MAXIMUM_PAR_ROLE) {
            count++;
        }
        isActive=true;
    }
    void countDecrement() {
        if(count!=Constants.NOMBRE_MINIMUM_PAR_ROLE) {
            count--;
            if(count==Constants.NOMBRE_MINIMUM_PAR_ROLE) {
                isActive=false;
            }
        }
    }

    Camps getCamp() {
        return camp;
    }
}
