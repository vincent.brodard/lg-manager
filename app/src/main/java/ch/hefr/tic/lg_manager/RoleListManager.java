package ch.hefr.tic.lg_manager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.ArrayList;
import java.util.List;

class RoleListManager {
    final List<Role> roles = new ArrayList<>();
    final List<Role> activeRoles = new ArrayList<>();
    final List<Role> deadRoles = new ArrayList<>();

    private final MainActivity mainActivity;

    static final Role aucunRole = new Role("Aucun",android.R.color.transparent, Camps.AUCUN);
    static final Role avecEvenements = new Role("Avec Événements",R.drawable.garde_champetre, Camps.AUCUN);
    static final Role avecBatiments = new Role("Avec Bâtiments",android.R.color.transparent, Camps.AUCUN);

    RoleListManager(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        roles.add(Roles.CHASSEUR.getValue(), new Role("Chasseur", R.drawable.chasseur, Camps.VILLAGE));
        roles.add(Roles.CUPIDON.getValue(), new Role("Cupidon", R.drawable.cupidon, Camps.VILLAGE));
        roles.add(Roles.PETITE_FILLE.getValue(), new Role("Petite Fille", R.drawable.petite_fille, Camps.VILLAGE));
        roles.add(Roles.SIMPLE_LOUP_GAROU.getValue(), new Role("Loup-Garou", R.drawable.simple_loup_garou, Camps.LOUPS));
        roles.add(Roles.SIMPLE_VILLAGEOIS.getValue(), new Role("Villageois", R.drawable.simple_villageois, Camps.VILLAGE));
        roles.add(Roles.SORCIERE.getValue(), new Role("Sorcière", R.drawable.sorciere, Camps.VILLAGE));
        roles.add(Roles.VOLEUR.getValue(), new Role("Voleur", R.drawable.voleur, Camps.VILLAGE));
        roles.add(Roles.VOYANTE.getValue(), new Role("Voyante", R.drawable.voyante, Camps.VILLAGE));
        roles.add(Roles.ANCIEN.getValue(), new Role("Ancien", R.drawable.ancien, Camps.VILLAGE));
        roles.add(Roles.BOUC_EMISSAIRE.getValue(), new Role("Bouc Émissaire", R.drawable.bouc_emissaire, Camps.VILLAGE));
        roles.add(Roles.IDIOT_DU_VILLAGE.getValue(), new Role("Idiot du Village", R.drawable.idiot_du_village, Camps.VILLAGE));
        roles.add(Roles.JOUEUR_DE_FLUTE.getValue(), new Role("Joueur de Flûte", R.drawable.joueur_de_flute, Camps.SOLITAITRE));
        roles.add(Roles.SALVATEUR.getValue(), new Role("Salvateur", R.drawable.salvateur, Camps.VILLAGE));
        roles.add(Roles.CORBEAU.getValue(), new Role("Corbeau", R.drawable.corbeau, Camps.VILLAGE));
        roles.add(Roles.LOUP_GAROU_BLANC.getValue(), new Role("Loup-Garou Blanc", R.drawable.loup_garou_blanc, Camps.SOLITAITRE));
        roles.add(Roles.PYROMANE.getValue(), new Role("Pyromane", R.drawable.pyromane, Camps.VILLAGE));
        roles.add(Roles.ABOMINABLE_SECTAIRE.getValue(), new Role("Abominable Sectaire", R.drawable.abominable_sectaire, Camps.SOLITAITRE));
        roles.add(Roles.ANGE.getValue(), new Role("Ange", R.drawable.ange, Camps.VILLAGE));
        roles.add(Roles.CHEVALIER_A_L_EPEE_ROUILLEE.getValue(), new Role("Chevalier à l'Épée rouillée", R.drawable.chevalier_a_l_epee_rouillee, Camps.VILLAGE));
        roles.add(Roles.CHIEN_LOUP.getValue(), new Role("Chien-Loup", R.drawable.chien_loup, Camps.LOUPS));
        roles.add(Roles.COMEDIEN.getValue(), new Role("Comédien", R.drawable.comedien, Camps.VILLAGE));
        roles.add(Roles.ENFANT_SAUVAGE.getValue(), new Role("Enfant Sauvage", R.drawable.enfant_sauvage, Camps.LOUPS));
        roles.add(Roles.FRERE.getValue(), new Role("Frère", R.drawable.frere, Camps.VILLAGE));
        roles.add(Roles.GITANE.getValue(), new Role("Gitane", R.drawable.gitane, Camps.VILLAGE));
        roles.add(Roles.GRAND_MECHANT_LOUP.getValue(), new Role("Grand Méchant Loup", R.drawable.grand_mechant_loup, Camps.LOUPS));
        roles.add(Roles.INFECT_PERE_DES_LOUPS.getValue(), new Role("Infect Père des Loups", R.drawable.infect_pere_des_loups, Camps.LOUPS));
        roles.add(Roles.JUGE_BEGUE.getValue(), new Role("Juge Bègue", R.drawable.juge_begue, Camps.VILLAGE));
        roles.add(Roles.MONTREUR_D_OURS.getValue(), new Role("Montreur d'Ours", R.drawable.montreur_d_ours, Camps.VILLAGE));
        roles.add(Roles.RENARD.getValue(), new Role("Renard", R.drawable.renard, Camps.VILLAGE));
        roles.add(Roles.SERVANTE_DEVOUEE.getValue(), new Role("Servante Dévouée", R.drawable.servante_devouee, Camps.VILLAGE));
        roles.add(Roles.SOEUR.getValue(), new Role("Soeur", R.drawable.soeur, Camps.VILLAGE));
        roles.add(Roles.VILLAGEOIS_VILLAGEOIS.getValue(), new Role("Villageois-Villageois", R.drawable.villageois_villageois, Camps.VILLAGE));
    }

    boolean isOneOrMoreWolfDead(){
        for (Role role:deadRoles){
            if(role.getCamp()==Camps.LOUPS){
                return true;
            }
        }
        return false;
    }

    void updateActiveRoles(){
        activeRoles.clear();
        for (Role role:roles){
            if(role.isActive()){
                activeRoles.add(role);
            }
        }
    }

    void updateDeadRoles(){
        deadRoles.clear();
        for (Role role:roles){
            if(role.isOneOrMoreDead()){
                deadRoles.add(role);
            }
        }
    }

    void checkVictoryCondition(Context context){
        if(!checkWolvesVictory(context)){
            if(!checkVillageVictory(context)){
                checkSolitaryVictory(context);
            }
        }
    }

    private boolean checkWolvesVictory(Context context){
        for (Role role : activeRoles){
            if (role.getCamp()!=Camps.LOUPS){
                return false;
            }
        }
        new AlertDialog.Builder(context)
                .setTitle("Les Loups-Garous ont gagné la partie")
                .setMessage("Vous pouvez commencer une nouvelle partie ou quitter l'application.")

                .setPositiveButton(R.string.Start_new_game, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.restart();
                    }
                })

                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })

                .setIcon(R.drawable.simple_loup_garou)
                .show();
        return true;
    }

    private boolean checkVillageVictory(Context context){
        for (Role role : activeRoles){
            if (role.getCamp()!=Camps.VILLAGE){
                return false;
            }
        }
        new AlertDialog.Builder(context)
                .setTitle("Le Village a gagné la partie")
                .setMessage("Vous pouvez commencer une nouvelle partie ou quitter l'application.")

                .setPositiveButton(R.string.Start_new_game, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.restart();
                    }
                })

                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(R.drawable.simple_villageois)
                .show();
        return true;
    }

    private void checkSolitaryVictory(Context context){
        Role crtRole = aucunRole;
        for (Role role : activeRoles){
            crtRole=role;
            if (role.getCamp()!=Camps.SOLITAITRE){
                    return;
            }
        }
        new AlertDialog.Builder(context)
                .setTitle("Le "+crtRole.getName()+" a gagné la partie")
                .setMessage("Vous pouvez commencer une nouvelle partie ou quitter l'application.")

                .setPositiveButton(R.string.Start_new_game, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mainActivity.restart();
                    }
                })

                .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(crtRole.getImage())
                .show();
    }
}
