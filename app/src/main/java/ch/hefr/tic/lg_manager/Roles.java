package ch.hefr.tic.lg_manager;

public enum Roles {
    CHASSEUR(0),
    CUPIDON(1),
    PETITE_FILLE(2),
    SIMPLE_LOUP_GAROU(3),
    SIMPLE_VILLAGEOIS(4),
    SORCIERE(5),
    VOLEUR(6),
    VOYANTE(7),
    ANCIEN(8),
    BOUC_EMISSAIRE(9),
    IDIOT_DU_VILLAGE(10),
    JOUEUR_DE_FLUTE(11),
    SALVATEUR(12),
    CORBEAU(13),
    LOUP_GAROU_BLANC(14),
    PYROMANE(15),
    ABOMINABLE_SECTAIRE(16),
    ANGE(17),
    CHEVALIER_A_L_EPEE_ROUILLEE(18),
    CHIEN_LOUP(19),
    COMEDIEN(20),
    ENFANT_SAUVAGE(21),
    FRERE(22),
    GITANE(23),
    GRAND_MECHANT_LOUP(24),
    INFECT_PERE_DES_LOUPS(25),
    JUGE_BEGUE(26),
    MONTREUR_D_OURS(27),
    RENARD(28),
    SERVANTE_DEVOUEE(29),
    SOEUR(30),
    VILLAGEOIS_VILLAGEOIS(31);

    private final int value;
    Roles(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
